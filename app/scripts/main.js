/*eslint-disable */
$(document).ready(function() {

  // Datepicker -----------------------------------------------

  $.datepicker.setDefaults($.extend($.datepicker.regional['ru']));

  $('.search__date').datepicker({
    dateFormat: "dd MM yy"
  }).datepicker("setDate", "0");

  // Spinner ---------------------------------------------------
  $.fn.spinner = function() {
    this.each(function() {
      var el = $(this);

      // add elements
      el.wrap('<span class="spinner"></span>');
      el.before('<span class="sub"></span>');
      el.after('<span class="add"></span>');

      // substract
      el.parent().on('click', '.sub', function() {
        if (el.val() > parseInt(el.attr('min')))
          el.val(function(i, oldval) {
            return --oldval;
          });
      });

      // increment
      el.parent().on('click', '.add', function() {
        if (el.val() < parseInt(el.attr('max')))
          el.val(function(i, oldval) {
            return ++oldval;
          });
      });
    });
  };

  $('input[type=number]').spinner();

  // Navigation ------------------------------------------------

  var hamburger = $('.nav__hamburger');
  var close = $('.nav__hamburger-close');
  var items = $('.nav__item').not('.nav__item--logo');

  hamburger.on('click', function(e) {
    e.preventDefault();
    items.slideToggle();
    close.toggle();
  });

  close.on('click', function(e) {
    e.preventDefault();
    items.slideToggle();
    close.toggle();
  });

  $(window).resize(function() {
    var wid = $(window).width();
    if (wid > 630) {
      items.removeAttr('style');
      close.css('display', 'none');
    }
  });

});
